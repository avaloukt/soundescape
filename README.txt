The main code I have developed for the SoundEScape app is organised and can be found in the following locations:

-Models:laravel/app/Models
-Controllers: laravel/app/Http/Controllers
-Views: laravel/resources/views
-Public css and javascript code: laravel/public

The web app is available at: http://54.194.105.17/alkistis/laravel/public