<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Place extends Model {

	protected $table = 'places';
	public $timestamps = false;


	public function ratings()
    {
        return $this->hasMany('App\Models\Rating', 'place_id');
    }

    public function cards()
    {
        return $this->hasMany('App\Models\Card', 'place_id');
    }

    

    
}
