<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Card extends Model {

	protected $table = 'cards';
    public $timestamps = false;


	public function place()
    {
        return $this->belongsTo('App\Models\Place', 'place_id');
    }

    public function sound1()
    {
        return $this->belongsTo('App\Models\Sound', 'sound_id_1');
    }

    public function sound2()
    {
        return $this->belongsTo('App\Models\Sound', 'sound_id_2');
    }
    
    public function sound3()
    {
        return $this->belongsTo('App\Models\Sound', 'sound_id_3');
    }

    public function sound4()
    {
        return $this->belongsTo('App\Models\Sound', 'sound_id_4');
    }

    public function sound5()
    {
        return $this->belongsTo('App\Models\Sound', 'sound_id_5');
    }

    

    
}