<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Sound extends Model {

	protected $table = 'sounds';
    public $timestamps = false;


	public function ratings()
    {
        return $this->hasMany('App\Models\Rating', 'sound_id');
    }

    public function cards_sound1()
    {
        return $this->hasMany('App\Models\Card', 'sound_id_1');
    }

      public function cards_sound2()
    {
        return $this->hasMany('App\Models\Card', 'sound_id_2');
    }

      public function cards_sound3()
    {
        return $this->hasMany('App\Models\Card', 'sound_id_3');
    }

      public function cards_sound4()
    {
        return $this->hasMany('App\Models\Card', 'sound_id_4');
    }

      public function cards_sound5()
    {
        return $this->hasMany('App\Models\Card', 'sound_id_5');
    }

    
}
