<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Rating extends Model {

	protected $table = 'ratings';
	public $timestamps = false;

    protected $fillable = array('id','sound_id', 'place_id', 'value');



	public function sound()
    {
        return $this->belongsTo('App\Models\Sound', 'sound_id');
    }

    public function place()
    {
        return $this->belongsTo('App\Models\Place', 'place_id');
    }

    

    
}