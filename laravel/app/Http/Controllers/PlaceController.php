<?php

namespace App\Http\Controllers;

use App\Models\Place;
use App\Models\Card;

use App\Http\Controllers\Controller;
use View;
use Session;


class PlaceController extends Controller {


	public function showPlaces()
	{
	    $places = Place::all(); 
	    $sessionToken = Session::get('_token');
        
	    //get all card with this session _token , eager load name relationship, 
	    $cardsOfUser = Card::with('place')->where('session_token', $sessionToken)->get();

	    //get only the places so only relations
	    $ratedPlacesArray = $cardsOfUser ->pluck('place.name');
	    $ratedPlacesArray = $ratedPlacesArray ->toArray();
	   



        return View::make('places', compact('places', 'ratedPlacesArray'));

	}


}