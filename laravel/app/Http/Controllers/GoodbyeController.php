<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use View;
use Session;



class GoodbyeController extends Controller {

	/**	
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function goodbye()
	{  
		Session::flush();

		$data = Session::all();

        return View::make('goodbye', compact('data'));

	}

}