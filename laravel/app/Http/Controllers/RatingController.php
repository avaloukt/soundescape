<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use App\Models\Card;
use App\Models\Sound;
use App\Http\Controllers\Controller;
use View;
use Request;



class RatingController extends Controller {

//pass cardId, pass array with sounds Ids fixed to work with array and less sounds
public function addSoundsToCard($cardId, $soundNames){

	$card = Card::find($cardId);

	$i=1;

	$table_col_names=['sound_id_1','sound_id_2', 'sound_id_3', 'sound_id_4', 'sound_id_5'];


	foreach ($soundNames as $soundName) {

			if($soundName != ''){
		
			$soundId = Sound::where('name', $soundName)->first()->id;

			$card->$table_col_names[($i-1)] = $soundId;
			$card->save();
			}

			$i++;	

		
	}


	$card->save();//might not be necessary, try this or the inner save 

}



//update or create the rating with plus one vote to its value
public function updateRating($placeId, $soundId){

// Retrieve the model by the attributes, or create it if it doesn't exist...
$rating = Rating::firstOrNew(array('sound_id' => $soundId, 'place_id' => $placeId));
$rating->value += 1; 
$rating->save();

}

public function updateRatingsForCard($cardId){

	$card = Card::find($cardId);

//update rating 1
if($card->sound_id_1 != null){

	 $this->updateRating($card->place_id, $card ->sound_id_1);
}

if($card->sound_id_2 != null){

	 $this->updateRating($card->place_id, $card ->sound_id_2);
}

if($card->sound_id_3 != null){

	$this->updateRating($card->place_id, $card ->sound_id_3);
}
if($card->sound_id_4 != null){

	$this->updateRating($card->place_id, $card ->sound_id_4);
}

if($card->sound_id_5 != null){

	$this->updateRating($card->place_id, $card ->sound_id_5);
}


}





public function ratingsCardHandler(){

//1. FILL THE CARD WITH FORM'S SOUNDS
//get the current cardId
$cardId = Request::get('cardId');
$soundNames = Request::get('soundNames');


//add the sounds selected to the card
$this->addSoundsToCard($cardId, $soundNames);


//2.. EAGER LOAD CARD WITH PLACE AND SOUNDS RELATIONSHIPS AND SHARE TO VIEW
$cardWithRelations = Card::with('place', 'sound1', 'sound2', 'sound3', 'sound4' , 'sound5')->find($cardId);
View::share('cardWithRelations',$cardWithRelations);

//get only sounds realationships and flatten them to array
 $relationsArray = $cardWithRelations ->relationsToArray();
 $soundRelationsArray = array_except($relationsArray, array('place'));
 //flatten with dot refernce the soundRelationsArray
 $soundsArray = array_dot($soundRelationsArray);

 View::share('soundsArray',$soundsArray);



//3. updateRatings with users choices
$this->updateRatingsForCard($cardId);



//4. GET  AND SHARE RATINGS get the collection of ratings of sounds existing for this place
//eager load ratings with sounds for this place
$ratings = Rating::with('sound')->where('place_id', $cardWithRelations->place_id)->get();
                                    
//sort the collection by the ratings value
$ratings = $ratings->sortBy('value')->reverse();

View::share('ratings',$ratings);




return View::make('ratings', compact('data'));



}


}

