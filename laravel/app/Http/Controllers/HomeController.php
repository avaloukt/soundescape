<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use View;
use Session;



class HomeController extends Controller {

	/**	
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function home()
	{  
		//$data = Session::all();


        return View::make('home');

	}

}