<?php

namespace App\Http\Controllers;

use App\Models\Sound;
use App\Models\Card;
use App\Models\Place;


use App\Http\Controllers\Controller;
use View;
use Request;
use Session;



class SoundController extends Controller {



	public function createCard($placeId){

		//create a new card entry with place and set used as 0

		$card = new Card;

		$card->place_id = $placeId;
		$card->used = '0';

		//save the session token of the card, this will make possible to group cards by session_token so belong to same user
		$card->session_token = Session::get('_token');
		$card->save();

		$cardId = $card->id;

		//return new id 
		return $cardId;

	}



	public function getSounds(){

		//create nested array with sounds by category
		$nature_sounds = Sound::where('category', 1)->get();
		$human_sounds = Sound::where('category', 2)->get();
		$mechanical_sounds= Sound::where('category', 3)->get();
		$music_sounds = Sound::where('category', 4)->get();


		$sounds = array(     
            'nature_sounds' => $nature_sounds,
            'human_sounds'=>$human_sounds,
            'mechanical_sounds'=>$mechanical_sounds,
            'music_sounds'=>$music_sounds,
        );
	          	
        return $sounds;
	}




	public function soundsCardHandler(){

		//get place name from input form and get place from db and share it to view
		$placeName = Request::get('placeName');
		$place = Place::where('name', $placeName)->first();

		View::share('place', $place);


		//create card with this place id and share its id among all views(next view) so can access it
		$cardId = $this->createCard($place->id);
		View::share('cardId', $cardId);

		//get and return to sounds view all sounds customly grouped
		$sounds = $this->getSounds();
	

		return View::make('sounds', $sounds);
	}

}