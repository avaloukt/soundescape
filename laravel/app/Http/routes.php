<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('home');
});

Route::get('home', 'HomeController@home');	
Route::get('places', 'PlaceController@showPlaces');
Route::post('sounds', 'SoundController@soundsCardHandler');
Route::post('ratings','RatingController@ratingsCardHandler');
Route::get('goodbye', 'GoodbyeController@goodbye');

	


	



