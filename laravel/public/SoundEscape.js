//js for sound tabs not necessary with JQuery and data-toggle
/*  e.preventDefault()
  $(this).tab('show')
})*/

$(document).ready(function(){

//called on click get the sound's name  and image in the next of the form's input that is empty
	var soundClickAction =function(){

			//get the id of the  clicked a with class = sound thumbnail
		var clicked_thumbnail_id = this.id;

		//get elements child img and its attributes
		//get whole img as child (obviously with attributes?)
        //selection like this for ids with spaces "div[id='content Module']"
		var img_source = $("[id='"+clicked_thumbnail_id+"']").find("img").attr("src");
		var img_alt = $("[id='"+clicked_thumbnail_id+"']").find("img").attr("alt");


		//make selected thumbnail bg green and disable all events attached to it
		$(this).addClass('alert-success');

		

		//table with all forms ids
		var forms_ids = ["#form1", "#form2", "#form3", "#form4", "#form5"];
		//var form_id;

		for(var i=0; i<5; i++){

			if( $(forms_ids[i]).val() == '' ){

				$(forms_ids[i]).val(clicked_thumbnail_id);
				//hide soundIconCase
				$(forms_ids[i]).siblings('.soundIconCase').hide();

				//insert the selected image
				$(document.createElement("img"))
   				 .attr({ src:img_source , alt:img_alt  })
				 .insertBefore($(forms_ids[i]));

				

				//insert an h4 with the name of the selected place before the hidden form element
				//!!! can not use insertAfter in firefox after a hidden element
				//	this has same result as below			$("<div class='caption'><h4>"+ $(forms_ids[i]).val() +"</h4></div>").insertBefore($(forms_ids[i]));


				$("<div class='caption text-center'><h4>"+ $(forms_ids[i]).val() +"</h4></div>").insertAfter($(forms_ids[i]).siblings("img"));

				//make x button visible
				$(forms_ids[i]).siblings('button').show();

				break;
			}
		}	

		//turn its self  this clicked sound thumbnail off, unbind from click event, disable its click
		$(this).off('click');


	}

//bind soundClickAction to all sounds thumbnails
   $('.thumbnail.sound').on('click', soundClickAction); 



//actions on click xbutton
$(".thumbnail.form").find("button").click(function(){

	//get image's alt which is its name and id for sound thumbnail in gallery
	sound_name = $(this).siblings("img").attr("alt");

	//ungreen selected sound thumbnail in gallery
	$("[id='"+sound_name+"']").removeClass("alert-success");

	//activate it, bind again soundClickAction to this sound thumbnail, enable it to be able to get clicked again 
	$("[id='"+sound_name+"']").on('click', soundClickAction); 

	//remove image inside  
	$(this).siblings("img").remove();

	//remover heading of sound's description
	$(this).siblings("div.caption").remove();

	//hide x button
	$(this).hide();
	//show sound icon of empty card
	$(this).siblings(".soundIconCase").show();
	//form value null
	$(this).siblings("input").val('');

 });





//on hover of sound thumbnail play the attached sound file
 $(".thumbnail.sound").hover(

 	function(){
 	//get the id of the  hovered tile append 'Sound' to make sound's id

   	var soundId= this.id +'Sound';
   	document.getElementById(soundId).play();

    },

    function(){

	var soundId= this.id +'Sound';
   	document.getElementById(soundId).pause();
    }


    );



//Places View: on click get the place's name in the form's input
    $('.thumbnail.place').click(function(){

	   	//get the id of the  clicked tile
		var clicked_thumbnail_id = this.id;

		//make bg green to show that is selected, ungreen other selected before
		$('.thumbnail.place').removeClass('bg-success');
    	$(this).addClass('bg-success');
		
		$('#placeInput').val(clicked_thumbnail_id);

	}); 
 
//place selected validator
$( "#placesForm" ).submit(function( event ) {
	if($('#placeInput').val() == ''){
		alert( "You need to select a place first! Click on a place's image to select it." );
  		event.preventDefault();

	}


});

 $('[data-toggle="popover"]').popover();




//to stop from writing text into text inputs
/*$( "input[type=text]" ).focus(function() {
  $( this ).blur();
});*/





});//end of on ready function



	
		