<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SoundEScape</title>
        <meta name="description" content="A prototype of a sound-tagging game of places of interest in Edinburgh.">
        <meta name="author" content="Alkistis Valouktsi">


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <link rel="stylesheet" type="text/css" href="SoundEscape.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> 

        <script src="SoundEscape.js"></script>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                        <h2>Thank you for contributing </h2>  

                </div>  
            </div><!--row1-->

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                            <p>For any questions, please contact me at avaloukt@gmail.com.
                        </p>
                        <p>Respectfully,
                            Alkistis Valouktsi.
                        </p>
                </div>  
            </div><!--row2-->

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">

                  <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-5 col-sm-offset-5 col-md-offset-5 col-lg-offset-5">
                    {!!link_to_action('HomeController@home', 'Play again', null ,$attributes = array('class'=>'btn btn-success btn-small')) !!}
                  </div>

                </div>
            </div> <!--row2--> 
        </div><!--container-->
    </body>
</html>
