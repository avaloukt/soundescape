<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SoundEScape</title>
        <meta name="description" content="A prototype of a sound-tagging game of places of interest in Edinburgh.">
        <meta name="author" content="Alkistis Valouktsi">


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <link rel="stylesheet" type="text/css" href="SoundEscape.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> 

        <script src="SoundEscape.js"></script>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                        <h3>Choose a place to sound scape</h3>
                </div>  
            </div><!--row1-->
           
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                        <div>
                          <h3><u>Places</u></h3>
                        </div>
                        <div id="places">
                            <div class="row">
                                @foreach($places as $place)

                                  @if(in_array($place->name, $ratedPlacesArray))
                                     <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 dynamicTile">
                                        <a href="#" class="thumbnail place selectedPlace" id="{{$place->name}}" 
                                                                  style="background-color:#e8e8e8;">
                                            <img src="{{'images/places/'.$place->image_url}}" alt="{{$place->name}}" style="opacity:0.4;">
                                                <div class="caption">
                                                    <h5 class="text-muted"> {{$place->name}}</h5>
                                                </div>
                                        </a>
                                    </div>
                                  @else
                                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 dynamicTile">
                                        <a href="#" class="thumbnail place" id="{{$place->name}}">
                                            <img src="{{'images/places/'.$place->image_url}}" alt="{{$place->name}}">
                                                <div class="caption">
                                                    <h5>{{$place->name}}</h5>
                                                </div>
                                        </a>
                                    </div>
                                  @endif  

                                @endforeach
                            </div>
                        </div>        
                </div>
            </div><!--row2-->

             <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                    <div class="form-group">
                        <div class="col-sm-2 col-md-2 col-lg-2 col-md-offset-10">


                        {!!Form::open(array('action' => 'SoundController@soundsCardHandler', 'id'=>'placesForm'))!!}


                        {!!Form::hidden('placeName',null,['class'=>'form-control', 'id'=>'placeInput'])!!}

                        {!!Form::submit('Next', ['class' => 'btn btn-lg btn-success'])!!}

                        {!!Form::close() !!}


                        </div>
                    </div>
                </div>
            </div> <!--row3-->
        </div>
    </body>
</html>

