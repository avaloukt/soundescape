<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SoundEScape</title>
        <meta name="description" content="A prototype of a sound-tagging game of places of interest in Edinburgh.">
        <meta name="author" content="Alkistis Valouktsi">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <link rel="stylesheet" type="text/css" href="SoundEscape.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> 

        <script src="SoundEscape.js"></script>

    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                        <h3>See how others rated this place before you</h3>
                </div>  
            </div><!--row2-->

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                        <h3>Sounds with Ratings</h3>

                        <div class="row"> <!--inner row-->

                          <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 col-sm-offset-2 col-md-offset-3 col-lg-offset-3">
                              <div class="thumbnail well">
                                  <img src="{{'images/places/'.$cardWithRelations->place->image_url}}" alt="{{$cardWithRelations->place->name}}" class="img-responsive">
                                    <div class="caption">
                                      <h4>{{$cardWithRelations->place->name}}</h4>
                                    </div>
                              </div>
                          </div>

                          <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 col-sm-offset-2 col-md-offset-3 col-lg-offset-3">
                            <table class="table">

                              <hr>
                                <td>
                                  <h4>Top sounds</h4>
                                </td>
                                <td>
                                </td>  
                                <td>
                                  <h4>Ratings</h4>
                                </td>

                              </hr>


                              @foreach($ratings as $rating) 

                                @if(in_array($rating->sound->name, $soundsArray) && ($rating->value==1))                           
                                <tr class="alert alert-info" data-toggle="popover" data-trigger="hover" data-content="New sound you contributed!" data-placement="top">
                                  <td>
                                    <div class"thumbnail">
                                      <img src="{{asset('images/sounds/'.$rating->sound->image_url)}}" alt="{{$rating->sound->name}}" class="img-responsive img-thumbnail">
                                    </div>
                                  </td>
                                  <td>
                                    <h4> {{$rating->sound->name}}</h4>
                                  </td>  
                                  <td>
                                    <h4> {{$rating->value}}</h4>
                                  </td>  
                                </tr>
                                @elseif(in_array($rating->sound->name,$soundsArray))
                                <tr class="alert alert-success" data-toggle="popover" data-trigger="hover" data-content="Sound you selected." data-placement="top">
                                  <td>
                                    <div class"thumbnail">
                                      <img src="{{asset('images/sounds/'.$rating->sound->image_url)}}" alt="{{$rating->sound->name}}" class="img-responsive img-thumbnail">
                                    </div>
                                  </td>
                                  <td>
                                    <h4> {{$rating->sound->name}}</h4>
                                  </td>  
                                  <td>
                                    <h4> {{$rating->value}}</h4>
                                  </td>  
                                </tr>
                                @else
                                <tr>
                                  <td>
                                    <div class"thumbnail">
                                      <img src="{{asset('images/sounds/'.$rating->sound->image_url)}}" alt="{{$rating->sound->name}}" class="img-responsive img-thumbnail">
                                    </div>
                                  </td>
                                  <td>
                                    <h4> {{$rating->sound->name}}</h4>
                                  </td>  
                                  <td>
                                    <h4> {{$rating->value}}</h4>
                                  </td>  
                                </tr>
                                @endif



                              
                             @endforeach

                            </table>
                          </div>  
                        </div><!--end of inner row-->    
                </div><!--col well -->
             </div><!--row2-->

             <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                  

                    <span class="col-xs-1 col-sm-1 col-md-1 col-lg-1 col-xs-offset-3 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
                      {!!link_to_action('GoodbyeController@goodbye', 'Quit', null ,$attributes = array('class'=>'btn btn-success btn-small')) !!}
                    </span>

                    <span class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-0">
                      {!!link_to_action('PlaceController@showPlaces', 'Continue soundscaping', null ,$attributes = array('class'=>'btn btn-success btn-small')) !!}
                    </span>

                </div>
             </div> <!--row3--> 

             

        </div><!--container-->



    </body>
</html>
