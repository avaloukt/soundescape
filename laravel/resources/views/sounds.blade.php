<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SoundEScape</title>
        <meta name="description" content="A prototype of a sound-tagging game of places of interest in Edinburgh.">
        <meta name="author" content="Alkistis Valouktsi">


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <link rel="stylesheet" type="text/css" href="SoundEscape.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> 

        <script src="SoundEscape.js"></script>

    </head>
    <body>
        <div class="container">
            <div class="row"><!--row1-->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                        <h3>Choose 5 sounds from the sound gallery that better describe  </h3>
                </div>  
            </div><!--row1-->

            <div class="row"><!--row2-->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">

                  <div class="container"><!--form's container-->
                    {!!Form::open(array('action' => 'RatingController@ratingsCardHandler'))!!}

                    <div clas="row">
                      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 thumbnail place" id="formsPlace">
                               <img src="{{'images/places/'.$place->image_url}}" alt="{{$place->name}}">
                                   <div class="caption">
                                      <h4>{{$place->name}}</h4>
                                   </div> 
                      </div>      

                      <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 form-group container"><!--form'sSounds Col-->
                        <div class="row"><!--form's Sounds Row-->

                          <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 col-sm-offset-1 thumbnail form formsSound">
                                    <div class="well soundIconCase">
                                      <span class="glyphicon glyphicon-volume-down"></span>
                                    </div>
                                    {!!Form::hidden('soundNames[]',null,['class'=>'form-control', 'id'=>'form1'])!!}
                                    <button type="button" class="btn btn-default btn-xs remove" aria-label="Delete" style="display:none">
                                         <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </button>                                     
                          </div> 

                          <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 thumbnail form formsSound">
                                   <div class="well soundIconCase">
                                      <span class="glyphicon glyphicon-volume-down"></span>
                                    </div>  
                                    {!!Form::hidden('soundNames[]',null,['class'=>'form-control', 'id'=>'form2'])!!}
                                    <button type="button" class="btn btn-default btn-xs remove" aria-label="Delete" style="display:none">
                                         <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </button>
                          </div> 

                          <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 thumbnail form formsSound">
                                   <div class="well soundIconCase">
                                      <span class="glyphicon glyphicon-volume-down"></span>
                                    </div>  
                                    {!!Form::hidden('soundNames[]',null,['class'=>'form-control', 'id'=>'form3'])!!}
                                    <button type="button" class="btn btn-default btn-xs remove" aria-label="Delete" style="display:none">
                                         <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </button>
                          </div> 

                          <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 thumbnail form formsSound">
                                   <div class="well soundIconCase">
                                      <span class="glyphicon glyphicon-volume-down"></span>
                                    </div>  
                                    {!!Form::hidden('soundNames[]',null,['class'=>'form-control', 'id'=>'form4'])!!}
                                    <button type="button" class="btn btn-default btn-xs remove" aria-label="Delete" style="display:none">
                                         <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </button>
                          </div> 
                          <div class="clearfix visible-xs-block"></div>

                          <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 thumbnail form formsSound">
                                   <div class="well soundIconCase">
                                      <span class="glyphicon glyphicon-volume-down"></span>
                                    </div>  
                                    {!!Form::hidden('soundNames[]',null,['class'=>'form-control', 'id'=>'form5'])!!}
                                    <button type="button" class="btn btn-default btn-xs remove" aria-label="Delete" style="display:none">
                                         <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </button>
                          </div> 
                          {!!Form::hidden('cardId',$cardId)!!} 
                          
                          {!!Form::hidden('placeName',null,['class'=>'form-control', 'id'=>'placeInput'])!!}

                                          
                          <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3 col-sm-offset-9 col-md-offset-9 col-lg-offset-9">
                             {!!Form::submit('Next', ['class'=>'btn btn-success btn-lg'])!!}
                          </div>
                          {!!Form::close() !!}
                        </div><!--end form's Sounds Row-->
                      </div><!-- end formsSounds Col--> 

                    </div><!--end forms row--> 
                  </div><!--end form's container-->
    
                </div><!--col div well-->
            </div><!--row2-->

           <div class="row"><!--row3-->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                    <h3>Sound Gallery</h3>
                          <!-- Nav tabs -->
                      <ul class="nav nav-tabs" id="soundTabs">
                            <li class="active"><a href="#nature" data-toggle="tab"><h5>Nature</h5></a></li>
                            <li><a href="#people" data-toggle="tab"><h5>People</h5></a></li>
                            <li><a href="#mechanical" data-toggle="tab"><h5>Mechanical</h5></a></li>
                            <li><a href="#music" data-toggle="tab"><h5>Music</h5></a></li>
                      </ul>

                          <!-- Tab panes -->
                      <div class="tab-content">   

                            <div class="tab-pane active fade in" id="nature">
                                <div class="row container">
                                     @foreach($nature_sounds as $sound)
                                        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2 dynamicTile">
                                            <a href="#" class="thumbnail sound" id="{{$sound->name}}">
                                                <img src="{{asset('images/sounds/'.$sound->image_url)}}" alt="{{$sound->name}}" class="img-responsive">
                                                 <audio preload="auto" id="{{$sound->name.'Sound'}}">
                                                   <source src="{{asset('soundSamples/nature/'.$sound->file_url)}}" type="audio/mpeg"></source>
                                                </audio>
                                                <div class="caption">
                                                    <h5>{{$sound->name}}</h5>
                                                </div>
                                            </a>
                                        </div>
                                      @endforeach
                                 </div>
                            </div>

                            <div class="tab-pane fade in" id="people">
                                <div class="row container">                                   
                                     @foreach($human_sounds as $sound)
                                        <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 dynamicTile">
                                            <a href="#" class="thumbnail sound" id="{{$sound->name}}">
                                                <img src="{{asset('images/sounds/'.$sound->image_url)}}" alt="{{$sound->name}}" class="img-responsive">
                                                 <audio preload="auto" id="{{$sound->name.'Sound'}}">
                                                   <source src="{{asset('soundSamples/human/'.$sound->file_url)}}" type="audio/mpeg"></source>
                                                </audio>
                                                <div class="caption">
                                                    <h5>{{$sound->name}}</h5>
                                                </div>
                                            </a>
                                        </div>
                                      @endforeach
                                 </div>
                            </div>

                            <div class="tab-pane fade in" id="mechanical">
                                <div class="row container">
                                     @foreach($mechanical_sounds as $sound)
                                        <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 dynamicTile">
                                            <a href="#" class="thumbnail sound" id="{{$sound->name}}">
                                                <img src="{{asset('images/sounds/'.$sound->image_url)}}" alt="{{$sound->name}}" class="img-responsive">
                                                 <audio preload="auto" id="{{$sound->name.'Sound'}}">
                                                   <source src="{{asset('soundSamples/mechanical/'.$sound->file_url)}}" type="audio/mpeg"></source>
                                                </audio>
                                                <div class="caption">
                                                    <h5>{{$sound->name}}</h5>
                                                </div>
                                            </a>
                                        </div>
                                      @endforeach
                                 </div>
                            </div>

                            <div class="tab-pane fade in" id="music">
                                <div class="row container">
                                     @foreach($music_sounds as $sound)
                                        <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2 dynamicTile">
                                            <a href="#" class="thumbnail sound" id="{{$sound->name}}">
                                                <img src="{{asset('images/sounds/'.$sound->image_url)}}" alt="{{$sound->name}}" class="img-responsive">
                                                 <audio preload="auto" id="{{$sound->name.'Sound'}}">
                                                   <source src="{{asset('soundSamples/music/'.$sound->file_url)}}" type="audio/mpeg"></source>
                                                </audio>
                                                <div class="caption">
                                                    <h5>{{$sound->name}}</h5>
                                                </div>
                                            </a>
                                        </div>
                                      @endforeach
                                 </div>
                            </div>

                      </div><!--end of tab content-->
                </div><!--column12-->
            </div><!--row3-->

        </div><!--container-->



    </body>
</html>
