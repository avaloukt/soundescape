<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SoundEScape</title>
        <meta name="description" content="A prototype of a sound-tagging game of places of interest in Edinburgh.">
        <meta name="author" content="Alkistis Valouktsi">


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <link rel="stylesheet" type="text/css" href="SoundEscape.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> 

        <script src="SoundEscape.js"></script>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                        <h2>Welcome to SoundEScape </h2>
                </div>  
            </div><!--row1-->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                        <p>SoundEScape is a prototype of a sound-tagging game of places of interest in Edinburgh, 
                            developed during Alkistis Valouktsi's MSc Design Informatics dissertation, in University of Edinburgh.
                        </p>
                        <p> The aim of this app is to create a map of the acoustic landscape of Edinburgh via collective sound-tagging.
                            Your choices will help shape the most representative sound set for each place.
                        </p>  
                        <p> Flow of the game:
                        </p>  
                        <ol>
                            <li>Choose one place to sound-tag.
                            </li>
                            <li>Choose 1-5 sounds that represent this place in your opinion
                            </li>
                            <li>See your sound contribution for this place and compare your choices with others
                            </li>
                            <li>Feel free to sound scape as many places you like
                            </li>
                        </ol>    

                </div>  
            </div><!--row1-->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
                        <h3>You can now start soundscaping your favourite places </h3>
                         <a class="btn btn-success" href="{{url('places')}}">Start</a>
                </div>  
            </div><!--row2-->
        </div><!--container-->
    </body>
</html>
